var express = require('express');
var router = express.Router();
var url = require('url');
var request = require('request');
var tesseract = require('tesseract_native');

/* GET home page. */
router.get('/', function(req, res, next) {
    var urlParts = url.parse(req.url, true);
    var query = urlParts.query;
    var imgUrl = query.url;
    var myOcr;

    request({uri: unescape(imgUrl), encoding: 'binary'}, function (err, response, body) {
        if (!err && res.statusCode == 200) {
            // So as encoding set to null then request body became Buffer object
            var base64prefix = 'data:' + response.headers['content-type'] + ';base64,';
            var image = new Buffer(body.toString(), 'binary').toString("base64");

            image = image;

            ocr = new tesseract.OcrEio();
            image = new Buffer(image, 'base64');

            //Process image
            try {
                ocr.ocr(image, function(err, result){
                    if(err){
                        res.render('index', { title: 'Express', error: err});
                    }else{
                        console.log(result);
                        res.render('index', { title: 'Express', text: result, url: imgUrl});
                    }
                });
            }catch(e){
                res.render('index', { title: 'Express', error: e});
            }
        } else {
            throw new Error('Can not download image');
        }
    });
});

module.exports = router;
